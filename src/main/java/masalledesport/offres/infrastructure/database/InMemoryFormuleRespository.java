package masalledesport.offres.infrastructure.database;

import masalledesport.offres.domain.formule.FormuleRepository;
import masalledesport.offres.domain.formule.Formule;

import java.util.Collection;

public class InMemoryFormuleRespository implements FormuleRepository {
    private Collection<Formule> formules;

    @Override
    public void persist(Formule formule) {
        formules.add(formule);
    }

    @Override
    public Formule find(String id) {
        return null;
    }
}
