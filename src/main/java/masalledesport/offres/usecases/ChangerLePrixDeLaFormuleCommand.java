package masalledesport.offres.usecases;

public final class ChangerLePrixDeLaFormuleCommand {
    String formuleId;
    String nouveauPrixDeBase;

    public ChangerLePrixDeLaFormuleCommand(String formuleId, String nouveauPrixDeBase) {
        this.formuleId = formuleId;
        this.nouveauPrixDeBase = nouveauPrixDeBase;
    }
}
