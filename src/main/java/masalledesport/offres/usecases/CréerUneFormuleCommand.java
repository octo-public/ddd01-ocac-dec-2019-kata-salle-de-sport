package masalledesport.offres.usecases;

import masalledesport.offres.domain.formule.Durée;

public class CréerUneFormuleCommand {
    String descriptif;
    String prix;
    Durée durée;

    public CréerUneFormuleCommand(String descriptif, String prix, Durée durée) {
        this.descriptif = descriptif;
        this.prix = prix;
        this.durée = durée;
    }
}
