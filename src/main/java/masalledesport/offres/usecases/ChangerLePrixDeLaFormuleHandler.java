package masalledesport.offres.usecases;

import masalledesport.EventPublisher;
import masalledesport.offres.domain.formule.Formule;
import masalledesport.offres.domain.formule.FormuleRepository;

public final class ChangerLePrixDeLaFormuleHandler {
    private final FormuleRepository formuleRepository;
    private final EventPublisher eventPublisher;

    public ChangerLePrixDeLaFormuleHandler(FormuleRepository formuleRepository, EventPublisher eventPublisher) {
        this.formuleRepository = formuleRepository;
        this.eventPublisher = eventPublisher;
    }

    public void execute(ChangerLePrixDeLaFormuleCommand command) {
        Formule formule = formuleRepository.find(command.formuleId);
        formule.changerLePrixDeBase(command.nouveauPrixDeBase);
        formuleRepository.persist(formule);
        eventPublisher.publish(formule.getDomainEvents());
    }
}
