package masalledesport.offres.usecases;

import masalledesport.EventPublisher;
import masalledesport.IdGenerator;
import masalledesport.offres.domain.formule.Formule;
import masalledesport.offres.domain.formule.FormuleRepository;

public class CréerUneFormuleHandler {
    private final IdGenerator idGenerator;
    private final FormuleRepository formuleRepository;
    private final EventPublisher eventPublisher;

    public CréerUneFormuleHandler(IdGenerator idGenerator, FormuleRepository formuleRepository, EventPublisher eventPublisher) {
        this.idGenerator = idGenerator;
        this.formuleRepository = formuleRepository;
        this.eventPublisher = eventPublisher;
    }

    public void execute(CréerUneFormuleCommand command) {
        Formule nouvelleFormule = Formule.nouvelle(
                idGenerator.generate(),
                command.descriptif,
                command.durée,
                command.prix
        );
        formuleRepository.persist(nouvelleFormule);
        eventPublisher.publish(nouvelleFormule.getDomainEvents());
    }
}
