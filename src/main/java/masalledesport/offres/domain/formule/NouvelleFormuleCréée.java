package masalledesport.offres.domain.formule;

import masalledesport.DomainEvents;

public final class NouvelleFormuleCréée implements DomainEvents {
    private final String id;

    public NouvelleFormuleCréée(String id) {
        this.id = id;
    }
}
