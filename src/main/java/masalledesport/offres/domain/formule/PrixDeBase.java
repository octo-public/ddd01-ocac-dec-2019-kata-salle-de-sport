package masalledesport.offres.domain.formule;

public final class PrixDeBase {
    private final String prix;

    public PrixDeBase(String prix) {
        this.prix = prix;
    }
}
