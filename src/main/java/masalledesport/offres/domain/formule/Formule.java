package masalledesport.offres.domain.formule;

import masalledesport.DomainEvents;

import java.util.List;

public final class Formule {
    private final FormuleId id;
    private final String descriptif;
    private final Durée durée;
    private PrixDeBase prixDeBase;
    private List<DomainEvents> domainEvents;

    public Formule(String id, String descriptif, Durée durée, String prixDeBase) {
        this.id = new FormuleId(id);
        this.descriptif = descriptif;
        this.durée = durée;
        this.prixDeBase = new PrixDeBase(prixDeBase);
    }

    public static Formule nouvelle(String id, String descriptif, Durée durée, String prixDeBase) {
        Formule formule = new Formule(id, descriptif, durée, prixDeBase);
        formule.domainEvents.add(new NouvelleFormuleCréée(formule.id.toString()));
        return formule;
    }

    public void changerLePrixDeBase(String nouveauPrixDeBase) {
        prixDeBase = new PrixDeBase(nouveauPrixDeBase);
        domainEvents.add(new LePrixDeBaseAChangé(id.toString(), nouveauPrixDeBase));
    }

    public List<DomainEvents> getDomainEvents() {
        return domainEvents;
    }
}
