package masalledesport.offres.domain.formule;

public interface FormuleRepository {
    void persist(Formule formule);

    Formule find(String id);
}
