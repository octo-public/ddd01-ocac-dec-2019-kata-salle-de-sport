package masalledesport.offres.domain.formule;

import masalledesport.DomainEvents;

public class LePrixDeBaseAChangé implements DomainEvents {
    private final String id;
    private final String nouveauPrixDeBase;

    public LePrixDeBaseAChangé(String id, String nouveauPrixDeBase) {
        this.id = id;
        this.nouveauPrixDeBase = nouveauPrixDeBase;
    }
}
