package masalledesport.offres.domain.formule;

public enum Durée {
    MOIS,
    ANNÉE
}
