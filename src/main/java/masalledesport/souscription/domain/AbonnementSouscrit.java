package masalledesport.souscription.domain;

import masalledesport.DomainEvents;

public class AbonnementSouscrit implements DomainEvents {
    private final String id;

    public AbonnementSouscrit(String id) {
        this.id = id;
    }

    public String getId() {
        return id;
    }

    public String getFormuleName() {
        return id;
    }

    public String getAbonnementPrix() {
        return id;
    }

    public String getEmail() {
        return id;
    }
}
