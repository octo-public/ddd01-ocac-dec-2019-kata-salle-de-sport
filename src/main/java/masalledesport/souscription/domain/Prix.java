package masalledesport.souscription.domain;

public class Prix {
    private final int prixDeBase;

    public Prix(String prixDeBase) {
        this.prixDeBase = Integer.decode(prixDeBase);
    }

    public double appliqueRéductions(Réduction réduction) {
        return prixDeBase * (1 - réduction.taux);
    }
}
