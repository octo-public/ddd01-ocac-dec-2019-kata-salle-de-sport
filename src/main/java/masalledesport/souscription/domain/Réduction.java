package masalledesport.souscription.domain;

import masalledesport.offres.domain.formule.Durée;

public class Réduction {
    public double taux = 0.0;

    public Réduction(Boolean étudiant, String formuleChoisieDurée) {
        if (étudiant) {
            taux = 0.3;
        }
        if (formuleChoisieDurée.equals(Durée.ANNÉE.toString())) {
            taux += 0.2;
        }
    }
}
