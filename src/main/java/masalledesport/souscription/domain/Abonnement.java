package masalledesport.souscription.domain;

import masalledesport.DomainEvents;

import java.util.ArrayList;
import java.util.List;

public class Abonnement {
    private final AbonnementId id;
    private final String formuleChoisieNom;
    private final String formuleChoisieDurée;
    private final String formuleChoisiePrixDeBase;
    private final String email;
    private final Boolean étudiant;
    public double prix;
    private List<DomainEvents> domainEvents = new ArrayList<>();

    public Abonnement(AbonnementId id, String formuleChoisieNom, String formuleChoisieDurée, String formuleChoisiePrixDeBase, String email, Boolean étudiant) {
        this.id = id;
        this.formuleChoisieNom = formuleChoisieNom;
        this.formuleChoisieDurée = formuleChoisieDurée;
        this.formuleChoisiePrixDeBase = formuleChoisiePrixDeBase;
        this.email = email;
        this.étudiant = étudiant;
    }

    public static Abonnement souscrire(String id, String formuleChoisieNom, String formuleChoisieDurée, String formuleChoisiePrixDeBase, String email, Boolean étudiant) {
        Abonnement abonnement = new Abonnement(new AbonnementId(id), formuleChoisieNom, formuleChoisieDurée, formuleChoisiePrixDeBase, email, étudiant);
        abonnement.prix = new Prix(formuleChoisiePrixDeBase)
                .appliqueRéductions(new Réduction(étudiant, formuleChoisieDurée));

        abonnement.domainEvents.add(new AbonnementSouscrit(abonnement.id.toString()));
        return abonnement;
    }

    public List<DomainEvents> getDomainEvents() {
        return null;
    }
}
