package masalledesport.souscription.domain;

public interface AbonnementRepository {
    void persist(Abonnement abonnement);
}
