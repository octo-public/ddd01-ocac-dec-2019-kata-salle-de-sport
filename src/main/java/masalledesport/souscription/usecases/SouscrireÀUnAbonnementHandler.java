package masalledesport.souscription.usecases;

import masalledesport.EventPublisher;
import masalledesport.IdGenerator;
import masalledesport.souscription.domain.Abonnement;
import masalledesport.souscription.domain.AbonnementRepository;

public final class SouscrireÀUnAbonnementHandler {
    private final IdGenerator idGenerator;
    private final AbonnementRepository abonnementRepository;
    private final EventPublisher eventPublisher;

    public SouscrireÀUnAbonnementHandler(IdGenerator idGenerator, AbonnementRepository abonnementRepository, EventPublisher eventPublisher) {
        this.idGenerator = idGenerator;
        this.abonnementRepository = abonnementRepository;
        this.eventPublisher = eventPublisher;
    }

    public void execute(SouscrireÀUnAbonnementCommand command) {
        Abonnement abonnement = Abonnement.souscrire(
                idGenerator.generate(),
                command.formuleChoisieNom,
                command.formuleChoisieDurée,
                command.formuleChoisiePrixDeBase,
                command.email,
                command.étudiant
        );
        abonnementRepository.persist(abonnement);
        eventPublisher.publish(abonnement.getDomainEvents());
    }
}
