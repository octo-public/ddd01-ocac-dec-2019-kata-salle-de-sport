package masalledesport.souscription.usecases;

public final class SouscrireÀUnAbonnementCommand {
    String formuleChoisieNom;
    String formuleChoisiePrixDeBase;
    String formuleChoisieDurée;
    String email;
    Boolean étudiant;

    public SouscrireÀUnAbonnementCommand(String formuleChoisieNom, String formuleChoisiePrixDeBase, String formuleChoisieDurée, String email, Boolean étudiant) {
        this.formuleChoisieNom = formuleChoisieNom;
        this.formuleChoisiePrixDeBase = formuleChoisiePrixDeBase;
        this.formuleChoisieDurée = formuleChoisieDurée;
        this.email = email;
        this.étudiant = étudiant;
    }
}
