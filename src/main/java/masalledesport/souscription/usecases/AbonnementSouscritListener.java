package masalledesport.souscription.usecases;

import masalledesport.souscription.domain.AbonnementSouscrit;

public class AbonnementSouscritListener {
    private final EnvoyerUnEmailDeRemerciementHandler commandHandler;

    public AbonnementSouscritListener(EnvoyerUnEmailDeRemerciementHandler handler) {
        this.commandHandler = handler;
    }

    void listen(AbonnementSouscrit event) {
        commandHandler.execute(new EnvoyerUnEmailDeRemerciementCommand(
                event.getEmail(),
                event.getFormuleName(),
                event.getAbonnementPrix()
        ));
    }
}
