package masalledesport;

import java.util.List;

public interface EventPublisher {
    void publish(List<DomainEvents> domainEvents);
}
