package masalledesport;

import masalledesport.offres.domain.formule.Durée;
import masalledesport.souscription.domain.Abonnement;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class TestAbonnement {
    @Test
    void aucune_réduction() {
        // When
        Abonnement abonnement = Abonnement.souscrire(
                "id",
                "formuleChoisieNom",
                Durée.MOIS.toString(),
                "100",
                "email",
                false
        );

        // Then
        assertEquals(100, abonnement.prix);
    }

    @Test
    void réduction_pour_les_étudiants_au_mois() {
        // When
        Abonnement abonnement = Abonnement.souscrire(
                "id",
                "formuleChoisieNom",
                Durée.MOIS.toString(),
                "100",
                "email",
                true
        );

        // Then
        assertEquals(70, abonnement.prix);
    }

    @Test
    void réduction_pour_les_étudiants_à_l_annnée() {
        // When
        Abonnement abonnement = Abonnement.souscrire(
                "id",
                "formuleChoisieNom",
                Durée.ANNÉE.toString(),
                "100",
                "email",
                true
        );

        // Then
        assertEquals(50, abonnement.prix);
    }

    @Test
    void réduction_à_l_annnée() {
        // When
        Abonnement abonnement = Abonnement.souscrire(
                "id",
                "formuleChoisieNom",
                Durée.ANNÉE.toString(),
                "100",
                "email",
                false
        );

        // Then
        assertEquals(80, abonnement.prix);
    }
}
