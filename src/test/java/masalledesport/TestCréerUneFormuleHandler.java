package masalledesport;

import masalledesport.offres.domain.formule.*;
import masalledesport.offres.usecases.CréerUneFormuleCommand;
import masalledesport.offres.usecases.CréerUneFormuleHandler;
import org.junit.jupiter.api.Test;

import java.util.List;

import static org.mockito.Mockito.*;

class FakeIdGenerator implements IdGenerator {
    @Override
    public String generate() {
        return "id";
    }
}

class TestCréerUneFormuleHandler {
    @Test
    void do_something() {
        // Given
        IdGenerator idGenerator = new FakeIdGenerator();
        FormuleRepository formuleRepository = mock(FormuleRepository.class);
        EventPublisher eventPublisher = mock(EventPublisher.class);
        CréerUneFormuleCommand command = new CréerUneFormuleCommand(
                "descriptif",
                "prix",
                Durée.ANNÉE
        );

        // When
        new CréerUneFormuleHandler(
                idGenerator,
                formuleRepository,
                eventPublisher
        ).execute(command);

        // Then
        verify(formuleRepository).persist(any(Formule.class));
        verify(eventPublisher).publish(anyListOf(DomainEvents.class));
    }
}
